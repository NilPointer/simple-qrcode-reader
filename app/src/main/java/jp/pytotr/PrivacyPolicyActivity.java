package jp.pytotr;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

import jp.pytotr.databinding.ActivityPrivacyPolicyBinding;
import jp.pytotr.gui.ApplicationColorTheme;
import jp.pytotr.process.log.DebugLogger;
import jp.pytotr.process.theme.ColorSetter;
import jp.pytotr.process.theme.ThemeGetter;

public class PrivacyPolicyActivity extends AppCompatActivity {
    private ActivityPrivacyPolicyBinding binding;
    private View thisView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPrivacyPolicyBinding.inflate(getLayoutInflater());
        thisView = binding.getRoot();
        setContentView(thisView);

        initPrivacyPolicy();
        initSunOrNightMode();
    }

    private void initSunOrNightMode() {
        ThemeGetter themeGetter = new ThemeGetter(this);
        ApplicationColorTheme theme = themeGetter.getTheme();

        thisView.setBackgroundColor(theme.ACTIVITY_BACKGROUND_COLOR);
        ColorSetter colorSetter = new ColorSetter(theme);
        colorSetter.setTextColor(binding.privacyPolicyText);
        colorSetter.setTextColor(binding.privacyPolicyTitle);
    }

    private void initPrivacyPolicy() {

        try (InputStream inputStream = getResources().openRawResource(R.raw.application_privacy_policy_jp);
             InputStreamReader streamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(streamReader);
        ) {

            StringBuilder stringBuilder = new StringBuilder();
            for (; ; ) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }

                stringBuilder.append(line);
                stringBuilder.append("\n");
            }

            binding.privacyPolicyText.setText(stringBuilder.toString());

        } catch (Exception e) {
            DebugLogger logger = new DebugLogger(this);
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            printWriter.flush();

            String stackTraceText = stringWriter.toString();

            logger.print(e, stackTraceText);
        }
    }
}