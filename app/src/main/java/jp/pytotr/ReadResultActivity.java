package jp.pytotr;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import jp.pytotr.databinding.ActivityReadResultBinding;
import jp.pytotr.gui.ApplicationColorTheme;
import jp.pytotr.process.manager.ActivityManager;
import jp.pytotr.process.manager.ToastManager;
import jp.pytotr.process.theme.ColorSetter;
import jp.pytotr.process.theme.ThemeGetter;

public class ReadResultActivity extends AppCompatActivity {
    private ActivityReadResultBinding binding;
    private View thisView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityReadResultBinding.inflate(getLayoutInflater());

        thisView = binding.getRoot();
        setContentView(thisView);
        initSumOrNightColor();

        String qrCodeText = ActivityManager.getIntentText(this, "READ").orElse("QRコードの読み取りに失敗しました");
        initTexts(qrCodeText);
        initButtons(qrCodeText);
    }

    private void initSumOrNightColor() {
        ThemeGetter themeGetter = new ThemeGetter(this);
        ApplicationColorTheme colorTheme = themeGetter.getTheme();

        thisView.setBackgroundColor(colorTheme.ACTIVITY_BACKGROUND_COLOR);

        ColorSetter setter = new ColorSetter(colorTheme);
        setter.setTextColor(binding.title);
        setter.setTextColor(binding.qrCodeEditText);
        setter.setTextColor(binding.qrCodeTextView);
        setter.setTextColor(binding.editTextTitle);

        setter.setButtonColor(binding.copyButton);
        setter.setButtonColor(binding.redoButton);
        setter.setButtonColor(binding.visibleButton);
    }

    private void initTexts(final String text) {
        binding.qrCodeEditText.setText(text);
        binding.qrCodeTextView.setText(text);

        binding.redoButton.setVisibility(View.INVISIBLE);
        binding.editTextTitle.setVisibility(View.INVISIBLE);
        binding.qrCodeEditText.setVisibility(View.INVISIBLE);
    }

    private void initButtons(final String text) {
        binding.redoButton.setOnClickListener(v -> binding.qrCodeEditText.setText(text));
        binding.visibleButton.setOnClickListener(v -> {
            binding.qrCodeEditText.setVisibility(View.VISIBLE);
            binding.editTextTitle.setVisibility(View.VISIBLE);
            binding.redoButton.setVisibility(View.VISIBLE);
        });

        binding.copyButton.setOnClickListener(v -> {
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

            if (clipboardManager == null) {
                ToastManager.toast(this, Toast.LENGTH_LONG, "クリップボードのコピーに失敗しました。お使いの端末では対応していない可能性があります");
                return;
            }

            String copyText = binding.qrCodeTextView.getText().toString();
            clipboardManager.setPrimaryClip(ClipData.newPlainText("", copyText));
            ToastManager.toast(this, Toast.LENGTH_SHORT, "QRコードの内容がクリップボードにコピーされました");
        });
    }
}