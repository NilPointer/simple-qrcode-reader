package jp.pytotr;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import jp.pytotr.databinding.ActivityMainBinding;
import jp.pytotr.gui.ApplicationColorTheme;
import jp.pytotr.gui.ApplicationNewBootAlertDialogFlagment;
import jp.pytotr.gui.PermissionAlertDialogFragment;
import jp.pytotr.process.check.VersionChecker;
import jp.pytotr.process.log.DebugLogger;
import jp.pytotr.process.manager.ActivityManager;
import jp.pytotr.process.manager.DialogManager;
import jp.pytotr.process.manager.ToastManager;
import jp.pytotr.process.permission.Permission;
import jp.pytotr.process.permission.PermissionChecker;
import jp.pytotr.process.permission.PermissionRequester;
import jp.pytotr.process.preference.LabelData;
import jp.pytotr.process.preference.Preferencer;
import jp.pytotr.process.theme.ColorSetter;
import jp.pytotr.process.theme.ThemeGetter;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private ApplicationColorTheme theme;
    private View thisView;

    private DebugLogger logger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        logger = new DebugLogger(this);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        thisView = binding.getRoot();
        setContentView(thisView);

        androidVersionCheck();
        initSunOrNightColor();
        initNightModeButton();
        initButtons();
        initTexts();
        initNewBootFragment();
    }

    /**
     * 読み取ったQRコードを受け取って表示アクティビティに移動する
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (!PermissionChecker.check(this)) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                ToastManager.toast(this, Toast.LENGTH_SHORT, "QRコードの読み取りに失敗しました");
            } else {
                ActivityManager.putAndMove(this, ReadResultActivity.class, "READ", result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    /**
     * パーミッションの許可不許可選択時に呼び出されるメソッド
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode != Permission.CAMERA.REQUEST_CODE) {
            return;
        }

        if (PermissionChecker.check(this)) {
            logger.print(this, "カメラ権限が許可された");
            new IntentIntegrator(this).initiateScan();

        } else {
            logger.print(this, "カメラ権限が許可されなかった");
            DialogManager.run(this, new PermissionAlertDialogFragment(), "permission");
        }
    }

    /**
     * minSDK未満のandroidならばアプリを終了させる
     */
    private void androidVersionCheck() {
        Preferencer preferencer = new Preferencer(this);
        final boolean checked = preferencer.isValue(LabelData.isApiChecked, false);

        logger.print(this, String.valueOf(checked));

        if (checked) {
            return;
        }

        VersionChecker checker = new VersionChecker(this);
        checker.check();

        preferencer.setValue(LabelData.isApiChecked, true);
    }

    /**
     * ナイトモードの設定を行う
     */
    private void initSunOrNightColor() {
        ThemeGetter themeGetter = new ThemeGetter(this);
        theme = themeGetter.getTheme();

        thisView.setBackgroundColor(theme.ACTIVITY_BACKGROUND_COLOR);

        final ColorSetter setter = new ColorSetter(theme);
        setter.setButtonColor(binding.qrCodeReadButton);
        setter.setButtonColor(binding.exitButton);
        setter.setButtonColor(binding.privacyPolicyButton);
        setter.setButtonColor(binding.ossLicenseButton);

        setter.setTextColor(binding.appName);
        setter.setTextColor(binding.footer);
    }

    /**
     * ナイトモードボタンが押されたときの動作
     */
    private void initNightModeButton() {

        binding.changeNightModeButton.setOnClickListener(v -> {
            theme = ApplicationColorTheme.getRebirth(theme);
            Preferencer preferencer = new Preferencer(this);
            preferencer.setValue(LabelData.isNightMode, theme.IS_NIGHT);

            initSunOrNightColor();
        });
    }

    private void initButtons() {
        binding.qrCodeReadButton.setOnClickListener(v -> PermissionRequester.request(this));
        binding.exitButton.setOnClickListener(v -> ActivityManager.finish(this));
        binding.ossLicenseButton.setOnClickListener(v -> ActivityManager.move(this, OssLicensesMenuActivity.class));
        binding.privacyPolicyButton.setOnClickListener(v -> ActivityManager.move(this, PrivacyPolicyActivity.class));
    }

    private void initTexts() {
        String builder = "このアプリはApache 2.0ライセンスによって\n" +
                "配布されている成果物を含んでいます\n\n" +
                "QRコードは株式会社デンソーウェーブの登録商標です。";
        binding.footer.setText(builder);
    }

    private void initNewBootFragment() {
        Preferencer preferencer = new Preferencer(this);
        final boolean newBooted = preferencer.isValue(LabelData.isNewBoot, true);

        if (newBooted) {
            DialogManager.run(this, new ApplicationNewBootAlertDialogFlagment(), "newBoot");
            preferencer.setValue(LabelData.isNewBoot, false);
        }
    }
}