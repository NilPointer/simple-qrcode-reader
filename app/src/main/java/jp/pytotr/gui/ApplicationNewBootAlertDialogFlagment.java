package jp.pytotr.gui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class ApplicationNewBootAlertDialogFlagment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("このアプリの戻るボタンについて");
        builder.setMessage("このアプリは戻るボタンを付けていません\n画面を戻る場合は、スマホについている戻るボタンを押してください");
        builder.setPositiveButton("OK", null);

        return builder.create();
    }


    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
