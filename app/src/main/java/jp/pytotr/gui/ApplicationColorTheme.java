package jp.pytotr.gui;

import android.graphics.Color;

public enum ApplicationColorTheme {
    SUN(Color.BLACK, Color.WHITE, Color.LTGRAY, false),
    NIGHT(Color.WHITE, Color.BLACK, Color.GRAY, true);

    public final int TEXT_COLOR;
    public final int ACTIVITY_BACKGROUND_COLOR;
    public final int BUTTON_BACKGROUND_COLOR;
    public final boolean IS_NIGHT;

    ApplicationColorTheme(int TEXT_COLOR, int ACTIVITY_BACKGROUND_COLOR, int BUTTON_BACKGROUND_COLOR, boolean IS_NIGHT) {
        this.TEXT_COLOR = TEXT_COLOR;
        this.ACTIVITY_BACKGROUND_COLOR = ACTIVITY_BACKGROUND_COLOR;
        this.BUTTON_BACKGROUND_COLOR = BUTTON_BACKGROUND_COLOR;
        this.IS_NIGHT = IS_NIGHT;
    }

    public static ApplicationColorTheme getRebirth(ApplicationColorTheme theme) {

        if (theme.equals(SUN)) {
            return NIGHT;
        } else {
            return SUN;
        }
    }
}
