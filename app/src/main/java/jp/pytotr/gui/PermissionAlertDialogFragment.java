package jp.pytotr.gui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class PermissionAlertDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("カメラ権限について");
        builder.setMessage(
                "カメラ権限が許可されませんでした。" +
                        "このアプリはカメラで読み取ったQRコード内容を表示するアプリです。" +
                        "そのため、権限なしでは何も出来ません。\n\n" +
                        "権限要求を二度と表示しないにチェックを入れた場合は設定から許可するか、" +
                        "再インストールをしてください\n\n" +
                        "アプリのプライバシーポリシに同意できない場合、直ちにアプリケーションをアンインストールしてください");
        builder.setPositiveButton("OK", null);

        return builder.create();
    }


    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
