package jp.pytotr.process.theme;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import jp.pytotr.gui.ApplicationColorTheme;

/**
 * テーマにあった色を設定するクラス
 */
public class ColorSetter {

    private final ApplicationColorTheme colorTheme;

    public ColorSetter(ApplicationColorTheme colorTheme) {
        this.colorTheme = colorTheme;
    }

    public void setButtonColor(Button button) {
        button.setTextColor(colorTheme.TEXT_COLOR);
        button.setBackgroundColor(colorTheme.BUTTON_BACKGROUND_COLOR);
    }

    public void setTextColor(TextView textView) {
        textView.setTextColor(colorTheme.TEXT_COLOR);
    }

    public void setTextColor(EditText editText) {
        editText.setBackgroundColor(colorTheme.BUTTON_BACKGROUND_COLOR);
        editText.setTextColor(colorTheme.TEXT_COLOR);
    }
}
