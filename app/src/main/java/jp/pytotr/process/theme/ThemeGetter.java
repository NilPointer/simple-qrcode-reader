package jp.pytotr.process.theme;

import androidx.appcompat.app.AppCompatActivity;

import jp.pytotr.gui.ApplicationColorTheme;
import jp.pytotr.process.preference.LabelData;
import jp.pytotr.process.preference.Preferencer;

/**
 * ダークテーマかどうかを判断して返す
 */
public class ThemeGetter {
    private final AppCompatActivity activity;

    public ThemeGetter(AppCompatActivity activity) {
        this.activity = activity;
    }

    public ApplicationColorTheme getTheme() {
        Preferencer preferencer = new Preferencer(activity);
        final boolean nightMode = preferencer.isValue(LabelData.isNightMode, false);

        ApplicationColorTheme theme;
        if (nightMode) {
            theme = ApplicationColorTheme.NIGHT;
        } else {
            theme = ApplicationColorTheme.SUN;
        }

        return theme;
    }
}
