package jp.pytotr.process.log;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import jp.pytotr.process.preference.LabelData;
import jp.pytotr.process.preference.Preferencer;

public class DebugLogger {
    private final boolean isLogPrint;

    public DebugLogger(AppCompatActivity activity) {
        Preferencer preferencer = new Preferencer(activity);

        //リリース時にはdefaultBoolをfalseにしてログを出力させない
        isLogPrint = preferencer.isValue(LabelData.isLogPrint, false);
    }

    public void print(Object o, String logText) {
        if (!isLogPrint) {
            return;
        }

        Class<?> objectClass = o.getClass();
        String className = objectClass.getName();

        Log.d("called " + className, logText);
    }
}
