package jp.pytotr.process.manager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

public class DialogManager {

    public static void run(AppCompatActivity activity, DialogFragment fragment, String tag) {
        fragment.show(activity.getSupportFragmentManager(), tag);
    }
}
