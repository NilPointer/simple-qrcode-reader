package jp.pytotr.process.manager;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ToastManager {

    public static void toast(AppCompatActivity appCompatActivity, int type, String toastText) {
        Toast.makeText(appCompatActivity, toastText, type).show();
    }
}
