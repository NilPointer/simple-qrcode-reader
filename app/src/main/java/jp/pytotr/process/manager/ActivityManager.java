package jp.pytotr.process.manager;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Optional;

public class ActivityManager {

    public static void finish(AppCompatActivity activity) {
        activity.finish();
    }

    public static void move(AppCompatActivity nowActivity, Class<? extends AppCompatActivity> moveActivity) {
        Intent intent = new Intent(nowActivity, moveActivity);
        nowActivity.startActivity(intent);
    }

    public static void putAndMove(AppCompatActivity nowActivity, Class<? extends AppCompatActivity> moveActivity, String label, String putText) {
        Intent intent = new Intent(nowActivity, moveActivity);
        intent.putExtra(label, putText);
        nowActivity.startActivity(intent);
    }

    public static Optional<String> getIntentText(AppCompatActivity movedNowActivity, String label) {
        Intent intent = movedNowActivity.getIntent();

        Optional<String> result;
        result = Optional.ofNullable(intent.getStringExtra(label));
        return result;
    }
}
