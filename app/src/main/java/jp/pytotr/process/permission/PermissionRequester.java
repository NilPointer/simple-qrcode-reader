package jp.pytotr.process.permission;

import android.Manifest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class PermissionRequester {

    /**
     * パーミッションを要求画面を出す
     *
     * @param activity 委譲元
     */
    public static void request(AppCompatActivity activity) {

        final String[] permissions = new String[]{Manifest.permission.CAMERA};
        ActivityCompat.requestPermissions(activity, permissions, Permission.CAMERA.REQUEST_CODE);
    }
}
