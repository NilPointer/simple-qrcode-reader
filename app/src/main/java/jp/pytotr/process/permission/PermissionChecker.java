package jp.pytotr.process.permission;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class PermissionChecker {

    /**
     * @param activity 委譲元
     * @return パーミッションが許可されていればtrue, 不許可ならfalse, 想定外の場合は例外を投げる
     */
    public static boolean check(AppCompatActivity activity) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            return false;
        } else {
            throw new IllegalStateException("Android Permission status is unknown.");
        }
    }
}
