package jp.pytotr.process.permission;

public enum Permission {
    CAMERA(1024);

    public final int REQUEST_CODE;

    Permission(int REQUEST_CODE) {
        this.REQUEST_CODE = REQUEST_CODE;
    }
}
