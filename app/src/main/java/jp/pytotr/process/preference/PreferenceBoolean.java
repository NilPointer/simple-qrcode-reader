package jp.pytotr.process.preference;

public enum PreferenceBoolean {
    FALSE(false, 0),
    TRUE(true, 1);

    public final boolean logical;
    public final int value;

    PreferenceBoolean(boolean logical, int value) {
        this.logical = logical;
        this.value = value;
    }

    public static PreferenceBoolean getInstance(int value) {
        ArgCheck.check(value > -1 && value < 2);

        if (value == 1) {
            return TRUE;
        }
        return FALSE;
    }

    public static PreferenceBoolean getInstance(boolean bool) {

        if (bool) {
            return TRUE;
        }
        return FALSE;
    }
}

class ArgCheck {
    public static void check(boolean logical) {

        if (!logical) {
            throw new IllegalArgumentException();
        }

    }
}