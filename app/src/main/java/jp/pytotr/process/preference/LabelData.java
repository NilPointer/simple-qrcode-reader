package jp.pytotr.process.preference;

public class LabelData {
    public static final String isApiChecked = "apiChecked";
    public static final String isLogPrint = "logPrint";
    public static final String isNightMode = "nightMode";
    public static final String isNewBoot = "newBoot";
}
