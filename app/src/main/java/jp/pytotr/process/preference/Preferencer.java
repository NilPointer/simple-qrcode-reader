package jp.pytotr.process.preference;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

public class Preferencer {
    private final AppCompatActivity activity;

    public Preferencer(AppCompatActivity activity) {
        this.activity = activity;
    }

    public int readInt(String key, int defaultValue) {
        SharedPreferences preferences = getSharedPreference(key);
        return preferences.getInt(key, defaultValue);
    }

    public void writeInt(String key, int writeValue) {
        SharedPreferences preferences = getSharedPreference(key);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, writeValue);
        editor.apply();
    }

    public boolean isValue(String key, boolean defaultBool) {
        PreferenceBoolean enumBoolean = PreferenceBoolean.getInstance(defaultBool);
        int result = readInt(key, enumBoolean.value);
        enumBoolean = PreferenceBoolean.getInstance(result);
        return enumBoolean.logical;
    }

    public void setValue(String key, boolean setBool) {
        PreferenceBoolean enumBoolean = PreferenceBoolean.getInstance(setBool);
        writeInt(key, enumBoolean.value);
    }

    private SharedPreferences getSharedPreference(String key) {
        return activity.getSharedPreferences(key, Context.MODE_PRIVATE);
    }


}
