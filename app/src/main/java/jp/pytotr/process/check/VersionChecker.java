package jp.pytotr.process.check;

import android.os.Build;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import jp.pytotr.process.log.DebugLogger;
import jp.pytotr.process.manager.ActivityManager;
import jp.pytotr.process.manager.ToastManager;

public class VersionChecker {
    private final AppCompatActivity activity;

    public VersionChecker(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void check() {

        int version = Build.VERSION.SDK_INT;
        DebugLogger logger = new DebugLogger(activity);
        logger.print(activity, "API version is" + version);

        if (version < 24) {
            ToastManager.toast(activity, Toast.LENGTH_SHORT, "android7 Nよりも古いAndroid には対応していません");
            ActivityManager.finish(activity);
        }

    }
}
